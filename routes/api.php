<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
  
use App\Http\Controllers\API\RegisterController;
use App\Http\Controllers\API\ProductController;
use App\Http\Controllers\FileUpload;
use App\Http\Controllers\AppointsController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
  
// Route::resource('fileupload', [FileuploadController::class, 'store']);

// Route::get('upload-file', [FileUpload::class, 'createForm']);

Route::post('upload-file', [FileUpload::class, 'fileUpload']);;

Route::post('register', [RegisterController::class, 'register']);

Route::post('login', [RegisterController::class, 'login']);

Route::post('update', [RegisterController::class, 'update']);

Route::post('createAppoint', [AppointsController::class, 'createAppoint']);

Route::get('getDates/{id}', [AppointsController::class, 'getDates']);

Route::get('getAppoints/{id}', [AppointsController::class, 'getAppoints']);

Route::middleware('auth:api')->group( function () {

    Route::resource('products', ProductController::class);

});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
