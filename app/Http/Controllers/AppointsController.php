<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\API\BaseController as BaseController;

use App\Models\Appoints;

use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\DB;

use Validator;

class AppointsController extends BaseController
{
    //http://localhost:8000/api/getDates/3
    public function getDates($doctorId){

        $dbData = DB::table('appoints')->where('doctorId', $doctorId)->pluck('appDate');

        return $this->sendResponse($dbData, 'Here is the data.');
    }

    public function getAppoints($doctorId){

        $dbData = DB::table('appoints')->where('doctorId', $doctorId)->get();

        return $this->sendResponse($dbData, 'Here is the data.');
    }

    public function createAppoint(Request $request){

        $validator = Validator::make($request->all(), [
            'doctorId' => 'required',
            'patientId' => 'required',
            'appDate' => 'required',
            'timeslot' => 'required',
        ]);
    
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
    
        $input = $request->all();
        $appoint = Appoints::create($input);
        $success['doctorId'] =  $appoint->doctorId;
        $success['patientId'] =  $appoint->patientId;
        $success['appDate'] =  $appoint->appDate;
        $success['timeslot'] =  $appoint->timeslot;
    
        return $this->sendResponse($success, 'User register successfully.');

    }

}
